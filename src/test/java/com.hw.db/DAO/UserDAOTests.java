package com.hw.db.DAO;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.mockito.Mockito;

import org.springframework.jdbc.core.JdbcTemplate;

import com.hw.db.models.User;

// Tests

final class UserDAOTests {

    // Private properties

    private JdbcTemplate mockJdbc;

    private final String nickname = "Aboba";

    private final String fullname = "Full Aboba";

    private final String email = "aboba@baboba.com";

    private final String about = "Aboba aboba aboba";

    // Setup methods

    @BeforeEach
    void setUp() {
        mockJdbc = Mockito.mock(JdbcTemplate.class);
        new UserDAO(mockJdbc);
    }

    // Test methods

    @Test
    void testNull() {
        UserDAO.Change(new User(nickname, null, null, null));
        Mockito.verify(mockJdbc, Mockito.never())
                .update(Mockito.any(String.class), Mockito.any(Object[].class));
    }

    @Test
    void testEmail() {
        UserDAO.Change(new User(nickname, email, null, null));
        Mockito.verify(mockJdbc)
                .update(
                        Mockito.eq("UPDATE \"users\" SET  email=?  WHERE nickname=?::CITEXT;"),
                        Mockito.eq(email),
                        Mockito.eq(nickname)
                );
    }

    @Test
    void testFullName() {
        UserDAO.Change(new User(nickname, null, fullname, null));
        Mockito.verify(mockJdbc)
                .update(
                        Mockito.eq("UPDATE \"users\" SET  fullname=?  WHERE nickname=?::CITEXT;"),
                        Mockito.eq(fullname),
                        Mockito.eq(nickname)
                );
    }

    @Test
    void testAbout() {
        UserDAO.Change(new User(nickname, null, null, about));
        Mockito.verify(mockJdbc)
                .update(
                        Mockito.eq("UPDATE \"users\" SET  about=?  WHERE nickname=?::CITEXT;"),
                        Mockito.eq(about),
                        Mockito.eq(nickname)
                );
    }

    @Test
    void testEmailAndFullName() {
        UserDAO.Change(new User(nickname, email, fullname, null));
        Mockito.verify(mockJdbc)
                .update(
                        Mockito.eq("UPDATE \"users\" SET  email=? , fullname=?  WHERE nickname=?::CITEXT;"),
                        Mockito.eq(email),
                        Mockito.eq(fullname),
                        Mockito.eq(nickname)
                );
    }

    @Test
    void testEmailAndAbout() {
        UserDAO.Change(new User(nickname, email, null, about));
        Mockito.verify(mockJdbc)
                .update(
                        Mockito.eq("UPDATE \"users\" SET  email=? , about=?  WHERE nickname=?::CITEXT;"),
                        Mockito.eq(email),
                        Mockito.eq(about),
                        Mockito.eq(nickname)
                );
    }

    @Test
    void testFullNameAndAbout() {
        UserDAO.Change(new User(nickname, null, fullname, about));
        Mockito.verify(mockJdbc)
                .update(
                        Mockito.eq("UPDATE \"users\" SET  fullname=? , about=?  WHERE nickname=?::CITEXT;"),
                        Mockito.eq(fullname),
                        Mockito.eq(about),
                        Mockito.eq(nickname)
                );
    }

    @Test
    void testEmailAndFullNameAndAbout() {
        UserDAO.Change(new User(nickname, email, fullname, about));
        Mockito.verify(mockJdbc)
                .update(
                        Mockito.eq("UPDATE \"users\" SET  email=? , fullname=? , about=?  WHERE nickname=?::CITEXT;"),
                        Mockito.eq(email),
                        Mockito.eq(fullname),
                        Mockito.eq(about),
                        Mockito.eq(nickname)
                );
    }

}
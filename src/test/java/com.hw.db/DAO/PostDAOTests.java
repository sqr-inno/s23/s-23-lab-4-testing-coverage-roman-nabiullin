package com.hw.db.DAO;

import org.junit.jupiter.api.BeforeEach;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.Timestamp;
import java.util.stream.Stream;

import com.hw.db.models.Post;
import static com.hw.db.DAO.PostDAO.setPost;

// Tests

final class PostDAOTests {

    // Private properties

    private JdbcTemplate mockJdbc;

    private final String responsePostAuthor = "Boba";

    private final String responsePostForum = "Boba chat";

    private final String responsePostMessage = "Hello, Boba!";

    private final Timestamp responsePostTimestamp = new Timestamp(1000);

    private static final String parameterPostAuthor = "Aboba";

    private static final String parameterPostForum = "Aboba chat";

    private static final String parameterPostMessage = "Hello, Aboba!";

    private static final Timestamp parameterPostTimestamp = new Timestamp(100);


    // Setup methods

    @BeforeEach
    void setUp() {
        mockJdbc = Mockito.mock(JdbcTemplate.class);
        new PostDAO(mockJdbc);
        Mockito.when(
            mockJdbc.queryForObject(
                Mockito.any(),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.any()
            )
        )
        .thenReturn(
            new Post(responsePostAuthor, responsePostTimestamp, responsePostForum, responsePostMessage, null, null, true)
        );
    }

    // Test methods

    @ParameterizedTest
    @MethodSource("parameterStream")
    void testMethodSetPost(Post post, String sql) {
        setPost(0, post);
        Mockito.verify(mockJdbc)
                .update(
                    Mockito.eq(sql),
                    ArgumentMatchers.<Object>any()
                );
    }

    // Private methods

    private static Stream<Arguments> parameterStream() {
        return Stream.of(
            Arguments.of(
                    new Post(parameterPostAuthor, parameterPostTimestamp, parameterPostForum, null, null, null, true),
                    "UPDATE \"posts\" SET  author=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"
            ),
            Arguments.of(
                    new Post(null, null, parameterPostForum, parameterPostMessage, null, null, true),
                    "UPDATE \"posts\" SET  message=?  , isEdited=true WHERE id=?;"
            ),
            Arguments.of(
                new Post(null, parameterPostTimestamp, parameterPostForum, parameterPostMessage, null, null, true),
                "UPDATE \"posts\" SET  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"
            ),
            Arguments.of(
                    new Post(parameterPostAuthor, null, parameterPostForum, null, null, null, true),
                    "UPDATE \"posts\" SET  author=?  , isEdited=true WHERE id=?;"
            ),
            Arguments.of(
                new Post(parameterPostAuthor, null, parameterPostForum, parameterPostMessage, null, null, true),
                "UPDATE \"posts\" SET  author=?  ,  message=?  , isEdited=true WHERE id=?;"
            ),
            Arguments.of(
                    new Post(parameterPostAuthor, parameterPostTimestamp, parameterPostForum, parameterPostMessage, null, null, true),
                    "UPDATE \"posts\" SET  author=?  ,  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"
            ),
            Arguments.of(
                    new Post(null, parameterPostTimestamp, parameterPostForum, null, null, null, true),
                    "UPDATE \"posts\" SET  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"
            )
        );
    }

}
package com.hw.db.DAO;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import org.springframework.jdbc.core.JdbcTemplate;

import org.mockito.Mockito;

import java.util.stream.Stream;

import static com.hw.db.DAO.ForumDAO.UserList;

// Tests

final class ForumDAOTests {

    // Private properties

    private JdbcTemplate mockJdbc;

    // Setup methods

    @BeforeEach
    void setUp() {
        mockJdbc = Mockito.mock(JdbcTemplate.class);
        new ForumDAO(mockJdbc);
    }

    // Test methods

    @ParameterizedTest
    @MethodSource("parameterStream")
    void testMethodUserList(Number limit, String since, Boolean desc, String sql) {
        UserList("slug", limit, since, desc);
        Mockito.verify(mockJdbc)
                .query(
                        Mockito.eq(sql),
                        Mockito.any(Object[].class),
                        Mockito.any(UserDAO.UserMapper.class)
                );
    }

    // Private methods

    // The format is (slug, limit, since_date, is_desc, query).
    private static Stream<Arguments> parameterStream() {
        return Stream.of(
                Arguments.of(null, null, false, "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname;"),
                Arguments.of(null, "10.10.2010", true, "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc;"),
                Arguments.of(5, "10.10.2010", false, "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname > (?)::citext ORDER BY nickname LIMIT ?;"),
                Arguments.of(5, "10.10.2010", true, "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc LIMIT ?;")
        );
    }

}

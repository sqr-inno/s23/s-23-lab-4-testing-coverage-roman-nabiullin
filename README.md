# Lab4 -- Testing coverage

- `UserDAOTests.java`: full mc/dc coverage for `UserDAO.Change`.

- `PostDaoTests.java`: full basis path coverage for `PostDAO.setPost`.

- `ForumDAOTests.java`: full branch coverage for `ForumDAO.UserList`.

- `ThreadDAOTests.java`: full statement coverage for `ThreadDAO.treeSort`.
